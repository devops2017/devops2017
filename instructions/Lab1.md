## Download and Install Virtualbox
https://www.virtualbox.org/wiki/Downloads

## Download ubuntu mini.iso 
http://archive.ubuntu.com/ubuntu/dists/trusty/main/installer-amd64/current/images/netboot/mini.iso

## Follow instructions to install ubuntu in virtualbox
Please use the same .iso download from above.

https://www.youtube.com/watch?v=MaAqAx77COM

Once the virtual machine is ready, shutdown and configure network adapters

https://www.youtube.com/watch?v=KZm-b34OnaY

Please Use following network adapters:
Adapter 1 : Bridged Network Adapter

Save the settings and start the VM

Note: Run: sudo apt-get install openssh-server
To be able to access the server via putty, or any ssh client.

## Install Git
Run command: sudo apt-get install git-all

## Create a Remote Repository
Go to https://bitbucket.org/ or https://github.com/ , create a new account and a repository.

## Follow Instructures provided in this webpage
https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup

## Understand git configs
https://git-scm.com/docs/git-repo-config/1.4.4
