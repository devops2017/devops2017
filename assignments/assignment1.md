#Git Assignment

##What is git ?

##Which is better Subversion(SVN) or GIT? why?

##How do you clone a remote repository?

##What is the difference between SSH and HTTP url in git clone?

##What must be done befoe you can clone using SSH protocol?

##What is the Difference between staging and commit ?
 
##When do you create a branch in git ?

##What are git hooks?

##Best branching strategy in git. Please explain.


#Basic Linux

##List 25 most used commands in linux, and explain what it does

##How do u search and replace in vi?


To submit the assignment

1. Clone repository - https://bishwash_devops@bitbucket.org/devops2017/devops2017.git

2. Switch to branch assignments

3. Create a directory assignments/your_firstname

4. Save answers under this directory as assignment1.txt

5. Push back to the repository.

If there are any issues, discuss with each other in HipChat.
I will provide feedback if necessary.